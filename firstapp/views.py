from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
import datetime

date = datetime.datetime.today()

times = ['year', 'day', 'month']

def hellodjango(reqeust: HttpRequest):
    return HttpResponse('Hello Django!')

def f_name(reqeust, name):
    return HttpResponse(f'Hello {name}!')

def date_time(reqeust):
    return HttpResponse(date.strftime('%d.%m.%Y'))

def date_times(reqeust, index):
    if index == times[0]:
        return HttpResponse(date.strftime('%Y'))
    elif index == times[1]:
        return HttpResponse(date.strftime('%d'))
    elif index == times[2]:
        return HttpResponse(date.strftime('%m'))