from django.urls import path
from .views import hellodjango, f_name, date_time, date_times

urlpatterns = [
    path('', hellodjango),
    path('date/', date_time),
    path('date/<str:index>/', date_times),
    path('<str:name>/', f_name)
]